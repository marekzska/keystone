import "./content.css";
import { Routes, Route } from "react-router-dom";
import Game from "./Game.js";
import Menu from "./Menu.js";

export default function Content() {
  return (
    <>
      <div id='wrap'>
        <div id='content'>
          <Routes>
            <Route exact path='/' element={<Menu />} />
            <Route exact path='/game/:gameId' element={<Game />} />
          </Routes>
        </div>
      </div>
    </>
  );
}
