import "./header.css";
import { Link } from "react-router-dom";
import Menu from "./Menu";

export default function Header() {
  return (
    <>
      <header>
        <Link to='/'>
          <h1 id='title'>.keystone</h1>
        </Link>
        <Link to='/' className='menu'></Link>
      </header>
    </>
  );
}
