import { useState, useEffect } from "react";
import Riddle from "./Riddle.js";
import { useParams } from "react-router-dom";
import './game.css';

export default function Game() {
  let { gameId } = useParams();
  const [value, setValue] = useState("");
  const [words, setWords] = useState([]);
  const [index, setIndex] = useState(0);

  useEffect(() => {
    fetch("/resources/words.json")
      .then((response) => response.json())
      .then((data) => {
        setWords(data.array.sort(() => Math.random() - 0.5));
      })
      .catch((err) => console.log(err));
  }, []);

  function handleKeyDown(e) {
    if (e.key === "Enter") {
      e.preventDefault();
      if (words[index].toUpperCase() === value.toUpperCase()) {
        setIndex((index) => index + 1);
      }
      setValue("");
    }
  }
  return (
    <>
      <div id='instruction'>
        <span id='instructionText'>
          trans
          <br />
          &nbsp;&nbsp;&nbsp;&nbsp;late
          <br />
          &nbsp;&nbsp;the w<br />
          ord
        </span>
      </div>
      <div id='riddle'>
        <Riddle game={gameId} text={words.length > 0 ? words[index] : ""} />
        <hr />
        <form id='answerForm'>
          <input
            type='text'
            value={value}
            placeholder='Your answer'
            onChange={(e) => {
              setValue(e.target.value);
            }}
            onFocus={(e) => (e.target.placeholder = "")}
            onBlur={(e) => (e.target.placeholder = "Your answer")}
            onKeyDown={handleKeyDown}
          />
        </form>
      </div>
    </>
  );
}
