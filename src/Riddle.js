import { useState, useEffect } from "react";
export default function Riddle(props) {
  const [morse, setMorse] = useState({});
  const [result, setResult] = useState("");

  useEffect(() => {
    if(props.game === 'morse'){
      fetch("/resources/morse.json")
        .then((response) => response.json())
        .then((data) => setMorse(data));
    }
  }, [props.game]);

  useEffect(() => {
    setResult('');
    if (Object.keys(morse).length > 0 && props.text.length > 0) {
      for (let i = 0; i < props.text.length; i++) {
        setResult((result) => result + morse[props.text[i]] + "/");
      }
    }
  }, [morse, props.text]);
  return (
    <>
      <span id='riddleText'>{result}</span>
    </>
  );
}
