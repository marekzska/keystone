import './menu.css';
import { Link } from 'react-router-dom';
export default function Menu() {
  return (
    <>
      <div id='menuWrap'>
        <Link to={"/game/morse"} className='menuItem'>
          MORSE CODE
        </Link>
        <Link to={"/game/mason"} className='menuItem'>
          MASONIC CIPHER
        </Link>
        <Link to={"/game/ceasar"} className='menuItem'>
          CEASAR CIPHER
        </Link>
      </div>
    </>
  );
}
