import Header from './Header.js';
import Content from './Content.js';

export default function App() {
  return (
    <>
      <Header />
      <Content />
    </>
  );
}
